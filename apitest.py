import unittest
import claritynowapi
import time

api = claritynowapi.ClarityNowConnection ('sfake','passfake','gsn-wb001.bla.org')

testcat1 = 'testcat1'
testcat2 = 'testcat2'
#debugCCM  = 1
#httpsPort = 443
#rmiServerPort = 52000

class ApiTestBase(unittest.TestCase):
    def setUp(self):
        api.prepareForAutomatedTest ()
        fss = api.getVolumes ()
        for fs in fss:
            if fs.name == 'test':
                api.deleteVolume (fs.id);

        categories = api.getTagCategories ()
        for cat in categories:
            if cat.name in [testcat1, testcat2]:
                api.deleteTagCategory (cat.id, True)

    def tearDown(self):                
        categories = api.getTagCategories ()
        for cat in categories:
            if cat.name in [testcat1, testcat2]:
                api.deleteTagCategory (cat.id, True)

    def createCat1(self):
        cat = claritynowapi.TagCategoryData (testcat1)
        id = api.addTagCategory (cat)

    def createCat2(self):
        cat = claritynowapi.TagCategoryData (testcat2)
        id = api.addTagCategory (cat)

class VolumeTests(unittest.TestCase):
    def setUp(self):
        api.prepareForAutomatedTest ()
        fss = api.getVolumes ()
        for fs in fss:
            if fs.name == 'test':
                api.deleteVolume (fs.id);

    def tearDown(self):
        pass

    def testAddDelete (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        api.deleteVolume (id)

    def testGetOne (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        fs = api.getVolume('test')
        self.assertEqual (fs.mount, 'c:\\')
        api.deleteVolume (id)

    def testGetAll (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        fss = api.getVolumes()
        for fs in fss:
            if fs.name == 'test': break
        else:
            self.fail ('failed to find test')
        api.deleteVolume (id)

    def testChange1 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        
        fs.subdir = "somedir"
        fs.hardLinkHandling = claritynowapi.VolumeData.COUNT_ALL
        fs.scanExclusionPattern = 'abcdef'
        fs.disableFolderLoopDetection = True
        fs.slowdownPercent = 10
        fs.scheduledScanThreads = 11
        fs.manualScanThreads = 12
        fs.eventTimeMinutes = 30
        fs.eventIntervalMinutes = 60
        fs.eventType = claritynowapi.ScannableData.SCAN_AT
        fs.eventDays = [True,False,False,False,False,False,True]

        api.changeVolume (fs)
        fs2 = api.getVolume('test')
        
        self.assertEqual (fs.subdir, fs2.subdir)
        self.assertEqual (fs.hardLinkHandling, fs2.hardLinkHandling)
        self.assertEqual (fs.scanExclusionPattern, fs2.scanExclusionPattern)
        self.assertEqual (fs.disableFolderLoopDetection, fs2.disableFolderLoopDetection)
        self.assertEqual (fs.slowdownPercent, fs2.slowdownPercent)
        self.assertEqual (fs.scheduledScanThreads, fs2.scheduledScanThreads)
        self.assertEqual (fs.manualScanThreads, fs2.manualScanThreads)
        self.assertEqual (fs.eventTimeMinutes, fs2.eventTimeMinutes)
        self.assertEqual (fs.eventIntervalMinutes, fs2.eventIntervalMinutes)
        self.assertEqual (fs.eventType, fs2.eventType)
        self.assertEqual (fs.eventDays, fs2.eventDays)

    def testChange2 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        
        fs.subdir = "somedir2"
        fs.hardLinkHandling = claritynowapi.VolumeData.COUNT_PRO_RATED
        fs.scanExclusionPattern = 'abcdefg'
        fs.disableFolderLoopDetection = False
        fs.slowdownPercent = 13
        fs.scheduledScanThreads = 14
        fs.manualScanThreads = 15
        fs.eventTimeMinutes = 90
        fs.eventIntervalMinutes = 120
        fs.eventType = claritynowapi.ScannableData.SCAN_INTERVAL
        fs.eventDays = [False,True,True,True,True,True,False]
        
        api.changeVolume (fs)
        fs2 = api.getVolume('test')
        
        self.assertEqual (fs.subdir, fs2.subdir)
        self.assertEqual (fs.hardLinkHandling, fs2.hardLinkHandling)
        self.assertEqual (fs.scanExclusionPattern, fs2.scanExclusionPattern)
        self.assertEqual (fs.disableFolderLoopDetection, fs2.disableFolderLoopDetection)
        self.assertEqual (fs.slowdownPercent, fs2.slowdownPercent)
        self.assertEqual (fs.scheduledScanThreads, fs2.scheduledScanThreads)
        self.assertEqual (fs.manualScanThreads, fs2.manualScanThreads)
        self.assertEqual (fs.eventTimeMinutes, fs2.eventTimeMinutes)
        self.assertEqual (fs.eventIntervalMinutes, fs2.eventIntervalMinutes)
        self.assertEqual (fs.eventType, fs2.eventType)
        self.assertEqual (fs.eventDays, fs2.eventDays)

class ScanGroupTests(unittest.TestCase):
    def setUp(self):
        api.prepareForAutomatedTest ()
        self.cleanup()

    def tearDown(self):
        self.cleanup()

    def cleanup(self):
        sgs = api.getScanGroups ()
        for sg in sgs:
            if sg.name == 'test':
                fss = api.getVolumesInScanGroup(sg.name)
                for fs in fss:
                    api.deleteVolume (fs.id);

    def testAddDelete(self):
        sg = claritynowapi.ScanGroupData('test')
        api.addScanGroup(sg)
        api.deleteScanGroup('test')

    def testScanGroupEnumeration(self):
        sg = claritynowapi.ScanGroupData('test')
        api.addScanGroup(sg)
        
        sgs = api.getScanGroups()
        self.assertEqual(1, len(sgs))
        self.assertEqual('test', sgs[0].name)

    def testScanGroupVolumes(self):
        # Create scan group.
        sg = claritynowapi.ScanGroupData('test')
        api.addScanGroup(sg)

        # Add volume.
        fs = claritynowapi.VolumeData ('testvol', 'c:\\')
        id = api.addVolumeToScanGroup ('test', fs)

        # Enumerate volumes in scan group.
        fss = api.getVolumesInScanGroup('test')        
        self.assertEqual(1, len(fss))
        self.assertEqual('testvol', fss[0].name)
    
class TagTests(ApiTestBase):
    def setUp(self):
        ApiTestBase.setUp(self)
        self.createCat1()
                
    def testAddDelete (self):
        tag = claritynowapi.TagData ('testtag')
        id = api.addTag (testcat1, tag)
        api.deleteTag (id)

    def testChange1 (self):
        tag = claritynowapi.TagData ('testtag')
        id = api.addTag (testcat1, tag)

        tag.expiration = 1234.0        

        api.changeTag (tag)
        tag2 = api.getTag(testcat1, 'testtag')
        
        self.assertEqual (tag.expiration, tag2.expiration)

    def testChange2 (self):
        tag = claritynowapi.TagData ('testtag')
        id = api.addTag (testcat1, tag)

        tag.expiration = 2234.0        

        api.changeTag (tag)
        tag2 = api.getTag(testcat1, 'testtag')
        
        self.assertEqual (tag.expiration, tag2.expiration)

    def testChange3 (self):
        tag = claritynowapi.TagData ('testtag')
        id = api.addTag (testcat1, tag)

        tag.expiration = None        

        api.changeTag (tag)
        tag2 = api.getTag(testcat1, 'testtag')
        
        self.assertEqual (tag.expiration, tag2.expiration)

    def testBulkAdd (self):
        cat = api.getTagCategory (testcat1)
        tag1 = claritynowapi.TagData ('testtag1')
        tag2 = claritynowapi.TagData ('testtag2')
        api.bulkTagUpdate ([[cat.name, tag1], [cat.name, tag2]], [], [])

        tagData1 = api.getTag(testcat1, 'testtag1')
        tagData2 = api.getTag(testcat1, 'testtag2')
        
    def testBulkUpdate (self):
        cat = api.getTagCategory (testcat1)
        tag1 = claritynowapi.TagData ('testtag1')
        tag2 = claritynowapi.TagData ('testtag2')
        api.bulkTagUpdate (tagsToAdd=[[cat.name, tag1], [cat.name, tag2]])

        tagData1 = api.getTag(testcat1, 'testtag1')
        tagData2 = api.getTag(testcat1, 'testtag2')

        tagData1.sizeLimit = 5000        
        tagData2.sizeLimit = 10000
        api.bulkTagUpdate (tagsToUpdate=[tagData1, tagData2])
        
        tagData1 = api.getTag(testcat1, 'testtag1')
        tagData2 = api.getTag(testcat1, 'testtag2')
        self.assertEqual (tagData1.sizeLimit, 5000)
        self.assertEqual (tagData2.sizeLimit, 10000)

    def testBulkDelete (self):
        cat = api.getTagCategory (testcat1)
        tag1 = claritynowapi.TagData ('testtag1')
        tag2 = claritynowapi.TagData ('testtag2')
        api.bulkTagUpdate (tagsToAdd=[[cat.name, tag1], [cat.name, tag2]])

        tagData1 = api.getTag(testcat1, 'testtag1')
        tagData2 = api.getTag(testcat1, 'testtag2')
        api.bulkTagUpdate (tagsToDelete=[tagData1.id, tagData2.id])

        tags = api.getTags (testcat1)
        for tag in tags:
            if tag.name in ['testtag1', 'testtag2']:
                self.fail('failed to delete tag')

class ImpliedTagTests(ApiTestBase):
    def setUp(self):
        ApiTestBase.setUp(self)
        self.createCat1()
        self.createCat2()
    
    def testAddDelete (self):
        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)
        
        tag2 = claritynowapi.TagData ('testtag2')
        tagId2 = api.addTag (testcat2, tag2)

        api.addImpliedTag (testcat1,'testtag',testcat2,'testtag2')
        api.removeImpliedTag (tagId, tagId2)
        
        api.deleteTag (tagId2)
        api.deleteTag (tagId)

    def testList (self):
        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)
        
        tag2 = claritynowapi.TagData ('testtag2')
        tagId2 = api.addTag (testcat2, tag2)

        api.addImpliedTag (testcat1,'testtag',testcat2,'testtag2')
        tags = api.getImpliedTags (testcat1,'testtag')
        self.assertEqual (tags[0].name, 'testtag2')
            
        api.removeImpliedTag (tagId, tagId2)
        
        api.deleteTag (tagId2)
        api.deleteTag (tagId)

    def testBulkGet (self):
        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)
        
        tag2 = claritynowapi.TagData ('testtag2')
        tagId2 = api.addTag (testcat2, tag2)

        api.addImpliedTag (testcat1,'testtag',testcat2,'testtag2')
        tags = api.bulkGetImpliedTags ([testcat1+'/testtag'])
        self.assertEqual (len(tags), 1)
        self.assertEqual (len(tags[0]), 1)
        self.assertEqual (tags[0][0], testcat2+"/testtag2")
            
        api.removeImpliedTag (tagId, tagId2)
        
        api.deleteTag (tagId2)
        api.deleteTag (tagId)

    def testBulkAddDelete (self):
        tag = claritynowapi.TagData ('testtag1')
        tagId = api.addTag (testcat1, tag)
        
        tag2 = claritynowapi.TagData ('testtag2')
        tagId2 = api.addTag (testcat2, tag2)

        api.bulkImpliedTagUpdate ([(testcat1+"/testtag1", [testcat2+"/testtag2"])], [])
        
        tags = api.getImpliedTags (testcat1,'testtag1')
        self.assertEqual(len(tags), 1)
        self.assertEqual (tags[0].id, tagId2)
        
        api.bulkImpliedTagUpdate ([], [(testcat1+"/testtag1", [testcat2+"/testtag2"])])

        tags = api.getImpliedTags (testcat1,'testtag1')
        self.assertEqual(len(tags), 0)


class FolderAttributeTests(ApiTestBase):
    def setUp(self):
        ApiTestBase.setUp(self)
        self.createCat1()
    
    def testGet (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        attributes = api.getFolderAttributes ('/test')
        self.assertEqual (attributes.expiration, None)
        self.assertEqual (attributes.sizeLimit, None)
        self.assertEqual (attributes.ignoreErrors, False)
        api.deleteVolume (id)

    def testSet1 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        attributes = claritynowapi.FolderAttributesData ()
        
        attributes.expiration = 1234.0
        attributes.sizeLimit = 500000
        attributes.ignoreErrors = False
        
        api.setFolderAttributes ('/test', attributes)
        attributes2 = api.getFolderAttributes ('/test')
        
        self.assertEqual (attributes.expiration, attributes2.expiration)
        self.assertEqual (attributes.sizeLimit, attributes2.sizeLimit)
        self.assertEqual (attributes.ignoreErrors, attributes2.ignoreErrors)
        
        api.deleteVolume (id)

    def testSet2 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        attributes = claritynowapi.FolderAttributesData ()
        
        attributes.expiration = 1235.0
        attributes.sizeLimit = 500001
        attributes.ignoreErrors = True
        
        api.setFolderAttributes ('/test', attributes)
        attributes2 = api.getFolderAttributes ('/test')
        
        self.assertEqual (attributes.expiration, attributes2.expiration)
        self.assertEqual (attributes.sizeLimit, attributes2.sizeLimit)
        self.assertEqual (attributes.ignoreErrors, attributes2.ignoreErrors)
        
        api.deleteVolume (id)

    def testSet3 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        id = api.addVolume (fs)
        attributes = claritynowapi.FolderAttributesData ()
        
        attributes.expiration = None
        attributes.sizeLimit = None
        attributes.ignoreErrors = False
        
        api.setFolderAttributes ('/test', attributes)
        attributes2 = api.getFolderAttributes ('/test')
        
        self.assertEqual (attributes.expiration, attributes2.expiration)
        self.assertEqual (attributes.sizeLimit, attributes2.sizeLimit)
        self.assertEqual (attributes.ignoreErrors, attributes2.ignoreErrors)
        
        api.deleteVolume (id)

    def testTagging (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        fsid = api.addVolume (fs)

        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)

        tags = api.getTagsForFolder ('/test')
        self.assertEqual (0, len(tags))

        tags.append (tagId)
        api.setTagsForFolder ('/test', tags)

        tags = api.getTagsForFolder ('/test')
        self.assertEqual (1, len(tags))
        self.assertEqual (tagId, tags[0])
        
        api.setTagsForFolder ('/test', [])
        tags = api.getTagsForFolder ('/test')
        self.assertEqual (0, len(tags))
        
        api.deleteTag (tagId, True)
        api.deleteVolume (fsid)

class FolderTests (ApiTestBase):
    def testFolderEnum (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        fsid = api.addVolume (fs)
        
        api.enumerateFolder ('/test')
        
        api.deleteVolume (fsid)

    def testScan (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')
        api.deleteVolume (fsid)

class ReportTests (ApiTestBase):
    def testVolumesTotals (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')
        request = claritynowapi.FastStatRequest ()
        request.resultType = claritynowapi.FastStatRequest.SUM

        subRequest = claritynowapi.SubRequest ()
        subRequest.name = "Volumes Totals"
        request.requests.append (subRequest)
        result = api.report (request)
        self.assertEqual (len(result.requests), 1)
        results = result.requests[0].results
        self.assertEqual (len(results), 3)
        self.assertEqual (results[0].name, "used")
        self.assertEqual (results[1].name, "unaccounted")
        self.assertEqual (results[2].name, "free")
        api.deleteVolume (fsid)

    def testTagByCategory (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        self.createCat1 ();

        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)

        tags = api.getTagsForFolder ('/test')
        self.assertEqual (0, len(tags))

        tags.append (tagId)
        api.setTagsForFolder ('/test', tags)

        category = api.getTagCategory (testcat1)
        request = claritynowapi.FastStatRequest ()
        request.resultType = claritynowapi.FastStatRequest.BY_CATEGORY

        request.groupByParameter = claritynowapi.GroupByCategory (category.id)

        subRequest = claritynowapi.SubRequest ()
        subRequest.name = "All Volumes"
        subRequest.filters.append (claritynowapi.TagFilter ([tagId]))
                                   
        request.requests.append (subRequest)
        result = api.report (request)
        self.assertEqual (len(result.requests), 1)
        results = result.requests[0].results
        for r in results:
            if r.name == "testtag":
                break
        else:
            self.fail("didn't find testtag")
        
        api.deleteVolume (fsid)
        api.deleteTag (tagId, True)

    def testPathResult (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')
        
        request = claritynowapi.FastStatRequest ()
        request.resultType = claritynowapi.FastStatRequest.ALL_PATHS

        subRequest = claritynowapi.SubRequest ()
        subRequest.name = "All Volumes"
        request.requests.append (subRequest)
        result = api.report (request)
        self.assertEqual (len(result.requests), 1)
        results = result.requests[0].results
        self.assertEqual (len(results), 1)
        self.assertEqual (results[0].paths[0].path, '/test')
        api.deleteVolume (fsid)

    def testExpandableVolumesFilter (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')
        request = claritynowapi.FastStatRequest ()
        request.resultType = claritynowapi.FastStatRequest.SUM

        subRequest = claritynowapi.SubRequest ()
        subRequest.name = "Volumes"
        subRequest.filters.append(claritynowapi.ExpandableVolumesFilter ())
        request.requests.append (subRequest)
        result = api.report (request)
        for request in result.requests:
            if request.name == "test":
                results = request.results
                
                self.assertEqual (len(results), 2)
                self.assertEqual (result.requests[0].name, "test")
                self.assertEqual (results[0].name, "used")
                self.assertEqual (results[1].name, "free")
                break
        else:
            self.fail("didn't find volume test")
            
        api.deleteVolume (fsid)

    def testExpandableCategoryFilter (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        self.createCat1 ();

        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)

        tags = api.getTagsForFolder ('/test')
        self.assertEqual (0, len(tags))

        tags.append (tagId)
        api.setTagsForFolder ('/test', tags)

        category = api.getTagCategory (testcat1)
        request = claritynowapi.FastStatRequest ()
        request.resultType = claritynowapi.FastStatRequest.SUM

        subRequest = claritynowapi.SubRequest ()
        subRequest.name = "All tags in category"
        subRequest.filters.append (claritynowapi.ExpandableCategoryFilter (category.id))
                                   
        request.requests.append (subRequest)
        result = api.report (request)
        self.assertEqual (len(result.requests), 1)
        self.assertEqual (result.requests[0].name, "testtag")
        
        api.deleteVolume (fsid)
        api.deleteTag (tagId, True)

class TagCategoryTests(ApiTestBase):
    def testAddDelete (self):
        cat = claritynowapi.TagCategoryData (testcat1)
        id = api.addTagCategory (cat)
        api.deleteTagCategory (id)

    def testAddDeleteVerify (self):
        cat = claritynowapi.TagCategoryData (testcat1)
        id = api.addTagCategory (cat)
        
        categories = api.getTagCategories ()
        for cat in categories:
            if cat.name in [testcat1]:
                break
        else:
            self.fail()
        api.deleteTagCategory (id)

    def testChange1 (self):
        cat = claritynowapi.TagCategoryData (testcat1)
        id = api.addTagCategory (cat)

        cat.mandatory = True
        api.changeTagCategory (cat)
        cat2 = api.getTagCategory(testcat1)
        
        self.assertEqual (cat.mandatory, cat2.mandatory)

        cat.mandatory = False
        api.changeTagCategory (cat)
        cat2 = api.getTagCategory(testcat1)
        
        self.assertEqual (cat.mandatory, cat2.mandatory)

class SearchFileInfoTests(ApiTestBase):
    def testSimpleSearch (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results,restartContext = api.searchForFileInfos ("testdir1", 10)
        self.assertEqual (restartContext, None)
        self.assertEqual (len(results), 1)
        self.assertEqual (results[0].name, "testdir1")
        
    def testSimpleSearch2 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results,restartContext = api.searchForFileInfos ("testdir", 10)
        self.assertEqual (restartContext, None)
        self.assertEqual (len(results), 2)
        self.assertEqual (results[0].name, "testdir1")
        self.assertEqual (results[1].name, "testdir2")

    def testLoop (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results = []
        restartContext = None
        while 1:
            infos,restartContext = api.searchForFileInfos ("testdir", 1, restartContext)
            results += infos
            if not restartContext:
                break;
        self.assertEqual (restartContext, None)
        self.assertEqual (len(results), 2)
        self.assertEqual (results[0].name, "testdir1")
        self.assertEqual (results[1].name, "testdir2")

class SearchPathsTests(ApiTestBase):
    def testSimpleSearch (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results,restartContext = api.searchForPaths ("testdir1", 10)
        self.assertEqual (restartContext, None)
        self.assertEqual (len(results), 1)
        self.assertEqual (results[0], "/test/testdir1")
        
    def testSimpleSearch2 (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results,restartContext = api.searchForPaths ("testdir", 10)
        self.assertEqual (restartContext, None)
        self.assertEqual (len(results), 2)
        self.assertEqual (results[0], "/test/testdir1")
        self.assertEqual (results[1], "/test/testdir2")

    def testLoop (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results = []
        restartContext = None
        while 1:
            infos,restartContext = api.searchForPaths ("testdir", 1, restartContext)
            results += infos
            if not restartContext:
                break;
        self.assertEqual (restartContext, None)
        self.assertEqual (len(results), 2)
        self.assertEqual (results[0], "/test/testdir1")
        self.assertEqual (results[1], "/test/testdir2")

    def testPathsGenerator (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results = []
        for r in api.searchForPathsGenerator("testdir"):
            results.append(r)
        self.assertEqual(results, ["/test/testdir1","/test/testdir2"])

    def testFileInfosGenerator (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\Download')
        fsid = api.addVolume (fs)
        api.scanSynchronous ('/test')

        results = []
        for r in api.searchForFileInfosGenerator("testdir"):
            results.append(r)
            
        self.assertEqual (len(results), 2)
        self.assertEqual (results[0].path, "/test/testdir1")
        self.assertEqual (results[1].path, "/test/testdir2")

class BulkTagGetTest(ApiTestBase):
    def setUp(self):
        ApiTestBase.setUp(self)
        self.createCat1()

    def testBulkTagGetTest (self):
        fs = claritynowapi.VolumeData ('test', 'c:\\')
        fsid = api.addVolume (fs)

        tag = claritynowapi.TagData ('testtag')
        tagId = api.addTag (testcat1, tag)

        tags = api.getTagsForFolder ('/test')
        self.assertEqual (0, len(tags))

        tags.append (tagId)
        api.setTagsForFolder ('/test', tags)

        results = api.bulkGetTagsForFolder(['/test'])
        self.assertEqual(1, len(results))
        self.assertEqual(1, len(results[0]))
        self.assertEqual("testcat1/testtag", results[0][0])
        
        api.deleteTag (tagId, True)
        api.deleteVolume (fsid)

class LdapGroupsTest(ApiTestBase):
    def setUp(self):
        ApiTestBase.setUp(self)

    def testGet (self):
        groups = api.getLdapGroupMapping()
        self.assertEqual(2, len(groups))
        for group in groups:
            self.assertEqual(2, len(group))
            cn_group, ldap_groups = group

    def testSet (self):
        groups = api.getLdapGroupMapping()
        self.assertEqual(2, len(groups))
        groups[0][1].append("ldap_group1")
        groups[0][1].append("ldap_group2")
        api.setLdapGroupMapping(groups)

        groups = api.getLdapGroupMapping()
        self.assertEqual(2, len(groups))
        self.assertEqual(2, len(groups[0][1]))

if __name__ == '__main__':
    unittest.main()
