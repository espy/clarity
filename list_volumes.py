#!/usr/bin/python
# This script must be set executable: chmod +x list_volumes.py
# Name:        list_volumes.py
# Description: Sample Python script to display capacities of all volumes
# Author:      Doug Schafer
# Copyright:   Copyright (C) 2018 DataFrameworks, Inc.
# Version:     1.0
# Date:        June 21, 2018

import sys

# Set up for remote access to API. For example:
#   on CN server /etc/exports:
#       /usr/local/claritynow/scripts/python  *(ro,insecure,all_squash,subtree_check)
#   on client running script
#       mkdir /mnt/cn-api
#       mount <CN server>:/usr/local/claritynow/scripts/python /mnt/cn-api
#sys.path.append('/mnt/cn-api')
# If running on local CN server, simply replace the above with
# sys.path.append('/usr/local/claritynow/scripts/python')

import claritynowapi

# CN Server settings
#CNserver = 'myCNserver.example.com'
CNserver = 'gsc-wb001.stjude.org'
Uname = 'sespycnapiuserFAKE'
Pass = 'passFAKE'

def main():
    # Insert username and password if not root and empty
    api = claritynowapi.ClarityNowConnection(Uname, Pass, CNserver)
    for volume in api.getVolumes():
        print volume.name
    
if __name__ == '__main__':
    main()

