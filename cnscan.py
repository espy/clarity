#!/usr/bin/env python

import sys, os
import configparser
import claritynowapi
import pymongo
import datetime

from pymongo import MongoClient


class CNScanner:
    cnusername = ''
    cnpassword = ''
    cnhost = ''
    api = None

    max = 13
    
    def scan(self):
        print('scanning')
        for volume in self.api.getVolumes():
            print('v=' + volume.name)
        print('done')
        
    def f(self):
        return 'test'
        
    def __init__(self, config):
        max = 22
        self.cnusername = config['cnusername']
        self.cnpassword = config['cnpassword']
        self.cnhost = config['cnhost']
        self.api = claritynowapi.ClarityNowConnection(self.cnusername, self.cnpassword, self.cnhost)

    
def handleFSObject(folderObject, api, fqPathName, directoryId, depth):
    
    if folderObject.fileType == 'FILE':
        #handleFile(folderObject, api, fqPathName, directoryId)
        pass
    elif folderObject.fileType == 'FOLDER':
        if depth < max_depth:
            handleDirectory(folderObject, api, fqPathName, directoryId, depth)
    else:
        print ("folderObject is of an unhandled type: " + folderObject.fileType + ' - ' + fqPathName + "/" + folderObject.name)
    pass

def handleDirectory(folderObject, api, fqPathName, directoryId, depth):
    depth += 1
    print ("depth = " + str(depth) + fqPathName + " - " + folderObject.name)
    
    #dirName = folderObject.name.encode('utf-8')
    #fqEscPathName = fqPathName + '/' + dirName
    
    fqPathName = fqPathName + '/' + folderObject.name
    
    fsdirs = getCollection('fsdir')  
    this_dir = fsdirs.find_one({"fq_path_name": fqPathName})
    
    if this_dir:
        directoryId = this_dir["_id"]
        #print("directory exists in db id = " + str(directoryId))
    else:
        directory = {"fq_path_name": fqPathName.encode('utf-8'),
            "size": folderObject.size,
            "dirCount": folderObject.dirCount,
            "fileCount": folderObject.fileCount,
            "lastCNScan": folderObject.lastScanned,
            "recordCreationDate": datetime.datetime.utcnow()}
            #"cumulativeLastModified": folderObject.cumulativeLastModified,
            #"lastModified": folderObject.lastModified,
        directoryId = fsdirs.insert_one(directory).inserted_id
        pass


    print ("directory="  + fqPathName)
    #folderContentList = api.enumerateFolderFromDb(fqPathName)
    folderContentList = api.enumerateFolder(fqPathName)
    for folderObject in folderContentList:
        handleFSObject(folderObject, api, fqPathName, directoryId, depth)
    
def handleFile(f, api, fqPathName, directoryId):
    fqPathName = fqPathName + '/' + f.name
    print ("file="  + fqPathName)
    fsfiles = getCollection('fsfile')
    file = {"fname": f.name,
            "bytes": f.size,
            "directoryId": directoryId,
            "lastCNScan": f.lastScanned,
            "tags": ["file", cnhost, "cnscan"],
            "recordCreationDate": datetime.datetime.utcnow()}
    putative = fsfiles.find_one({"fname": f.name, "directoryId": directoryId})
    if not putative:
        fileId = fsfiles.insert_one(file).inserted_id
    
def getCollection(collection_name):
    client = MongoClient('localhost', 27017)
    db = client['fsdb']
    fsfiles = db[collection_name]
    return fsfiles
    
def dscan(fqPathName, api):
    #TODO faking a directory as a start point
    #startingWith = volume.name
    
    #fqPathName = '/' + fqPathName
    #fqPathName = "/rgs01/home/clusterHome/sespy/opt"
    #fqPathName = "/rgs01/projects/RO"
    print ('scanning path ' + fqPathName)
    folderContentList = api.enumerateFolderFromDb(fqPathName)
    for folderObject in folderContentList:
        handleFSObject(folderObject, api, fqPathName, None, 0)
    print ('done scanning path ' + fqPathName)

def vscan(volume, api, scanner):
    print ('begin scan of volume ' + volume.name)
    print scanner.max
    startingWith = '/' + volume.name
    startingWith = '/rgs01/home/clusterHome/hpan'
    dscan(startingWith, api)
    print ('end scan of volume ' + volume.name)
    
def do_cnscan(config):
    if config['nuke'] == 'True':
        nuke()
    scanner = CNScanner(config)
    #TODO walk through a list of configs, since a scan might
    # be multiple CN servers.
    
    #for volume in scanner.api.getVolumes():
    #    vscan(volume, scanner.api, scanner)
    scanner.scan()
        
def nuke():
    print('nuking this database')
    fsfiles = getCollection('fsfile')
    fsdirs = getCollection('fsdir')    
    fsfiles.drop()
    fsdirs.drop()
    print('nuked.')

    

