#!/usr/bin/env python
import sys, os
import datetime
import cnscan, configparser

from pymongo import MongoClient


def makeConfigDict(configFileNameAndPath, configSetName):
	config = configparser.ConfigParser()
	config.read(configFileNameAndPath)
	d = config[configSetName]
	return d
	
def usage():
	print ("Usage:")
	print ("\t")
	print ("\t run.py myConfigFileName.config")
	print ("\t")

def filePresent(filePathAndName):
    if not os.path.exists(filePathAndName):
        print("path does not exist " + filePathAndName)
        return False
    if not os.path.isfile(filePathAndName):
        print("file does not exist " + filePathAndName)
        return False    
    return True
    
    
def assureArgCount(args, expectedCount):
    if len(sys.argv) < expectedCount:
        usage()
        exit(1)
        
def main():
    #print('begin')
    args = sys.argv
    assureArgCount(args, 2)
    configFileIn = sys.argv[1]
    if filePresent(configFileIn):
        #config = haibIntake.setupConfig(configFileIn)
        config = makeConfigDict(configFileIn, "DEFAULT")
    else:
        configPath ='./'
        defaultConfigFileNameAndPath = configPath + "default.config"
        if filePresent(defaultConfigFileNameAndPath):
            config = makeConfigDict(defaultConfigFileNameAndPath, "DEFAULT")
        else:
            print('failed to read config file')
            exit(1)
    if '--nuke=true' in args:
    	config['nuke'] = 'True'
    else:
    	config['nuke'] = 'False'
    cnscan.do_cnscan(config)
    #print('end')

if __name__ == '__main__':
    main()
